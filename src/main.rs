use macroquad::prelude::*;

#[macroquad::main("Ray Tracing")]
async fn main() {
    let texture = render_target(8, 8).texture;
    let material = load_material(
        ShaderSource::Glsl {
            vertex: include_str!("vert_shader.glsl"),
            fragment: include_str!("frag_shader.glsl"),
        },
        MaterialParams {
            uniforms: vec![
                ("screen_resolution".to_owned(), UniformType::Float2),
                ("fov".to_owned(), UniformType::Float1),
                ("poses".to_owned(), UniformType::Float3),
                ("radiuses".to_owned(), UniformType::Float1),
            ],
            ..Default::default()
        },
    )
    .unwrap();

    set_default_camera();
    loop {
        clear_background(WHITE);

        gl_use_material(&material);
        material.set_uniform("screen_resolution", (screen_width(), screen_height()));
        material.set_uniform("fov", 180f32);
        material.set_uniform("poses", [(0f32, 0f32, -3f32), (0f32, 1f32, -3f32)]);
        material.set_uniform("radiuses", [1f32, 1.5]);

        draw_texture(&texture, -1f32, -1f32, WHITE);
        gl_use_default_material();

        next_frame().await;
    }
}
