uniform vec2 screen_resolution;

attribute vec2 position;
varying vec2 uv;
varying vec2 sc_res;

void main() {
  gl_Position = vec4(position, 0.0, 1.0);
  uv = (position + 1.0) / 2.0;
  sc_res = screen_resolution;
}
