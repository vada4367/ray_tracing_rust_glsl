uniform float fov;
uniform vec3 poses[2];
uniform float radiuses[2];
varying vec2 uv;
varying vec2 sc_res;

struct Camera {
  vec3 position;
  vec3 direction;
  float fov;
};

struct Ray {
  vec3 origin;
  vec3 direction;
  vec3 color;
};

struct Sphere {
  vec3 position;
  float radius;
};

Ray ray_new(Camera camera) {
  Ray ray;
  ray.origin = camera.position;
  ray.direction = normalize(vec3((uv.x - 0.5) * sc_res.x, (uv.y - 0.5) * sc_res.y, sc_res.x / (2.0 * tan(camera.fov / 2.0))));
  ray.color = vec3(1.0);
  return ray;
}

float sphere(Ray ray, Sphere sphere) {
  vec3 oc = ray.origin - sphere.position;
  float a = dot(ray.direction, ray.direction);
  float b = dot(oc, ray.direction);
  float c = dot(oc, oc) - sphere.radius * sphere.radius;
  
  float discriminant = b * b - a * c;
  
  if (discriminant < 0.0) {
    return -1.0;
  } else {
    float t = (-b + sqrt(discriminant)) / a;
    return t;
  }
}

void main() {
  Camera camera;
  camera.position = vec3(0.0);
  camera.direction = vec3(0.0);
  camera.fov = fov;

  Ray ray = ray_new(camera);
  float min_t = -1.0;
  for (int i = 0; i < 2; i++) {
    Sphere sphere;
    sphere.position = poses[i];
    sphere.radius = radiuses[i];
    
    float t = sphere(ray, sphere);
    if (t > 0.0 && (t < min_t || min_t == -1.0)) {
      min_t = t;
    }
  }

  // float inter = ray.origin + ray.direction * t;

  if (min_t > 0.0) {
    gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
  } else {
    gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
  }
}
